#!/bin/sh

set -e -u -x

REGISTRY="registry.gitlab.com"
TAG="twied/dnd5ehelper"

podman login "${REGISTRY}"
podman build -t "${REGISTRY}/${TAG}" .
podman push "${REGISTRY}/${TAG}"
