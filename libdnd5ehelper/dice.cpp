// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "dice.h"

#include <fmt/core.h>
#include <yaml-cpp/yaml.h>

#include <random>
#include <sstream>
#include <stdexcept>

namespace dnd5e {

class DefaultRandom : public Random {
public:
    int operator()(int from, int to) override {
        std::uniform_int_distribution<> dist { from, to };
        return dist(m_generator);
    }

private:
    std::mt19937 m_generator { std::random_device {}() };
};

static DefaultRandom default_random {};

Random& Random::instance() {
    return default_random;
}

DiceRoll& DiceRoll::add_dice(int sides, int result) {
    m_dice.emplace(sides, result);
    return *this;
}

int DiceRoll::sum() const noexcept {
    int sum {};

    for (const auto& [sides, result] : m_dice) {
        sum += result;
    }

    return sum;
}

std::string DiceRoll::to_string() const noexcept {
    std::string retval {};
    int bonus {};

    for (const auto& [sides, result] : m_dice) {
        if (sides == 1) {
            bonus += result;
            continue;
        }

        retval += fmt::format("{}/{}, ", result, sides);
    }

    if (bonus != 0) {
        retval += fmt::format("{}/1, ", bonus);
    }

    retval += fmt::format("total: {}", sum());
    return retval;
}

DiceSet DiceSet::parse(const std::string& string) {
    std::stringstream stream { string };
    return parse(stream);
}

DiceSet DiceSet::parse(std::istream& stream) {
    enum class Type {
        eof,
        unknown,
        plus,
        minus,
        d,
        number
    };

    struct Token {
        Type type;
        int value;
    };

    int c { ' ' };
    auto read = [&c, &stream]() {
        while (true) {
            if (c < 0) {
                return Token { Type::eof, 0 };
            }

            if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {
                c = stream.get();
                continue;
            }

            if (c == '+') {
                c = stream.get();
                return Token { Type::plus, 0 };
            }

            if (c == '-') {
                c = stream.get();
                return Token { Type::minus, 0 };
            }

            if (c == 'd') {
                c = stream.get();
                return Token { Type::d, 0 };
            }

            if (c < '0' || c > '9') {
                throw std::runtime_error {
                    fmt::format("unexpected char '{c}'", c)
                };
            }

            Token retval { Type::number, 0 };
            while (c >= '0' && c <= '9') {
                retval.value = retval.value * 10 + c - '0';
                c = stream.get();
            }

            return retval;
        }
    };

    Token token = read();
    auto eat = [&token, &read](Type t) {
        const int retval = token.value;

        if (token.type != t) {
            throw std::runtime_error("unexpected token");
        }

        token = read();
        return retval;
    };

    int last_sign = '+';

    DiceSet retval;
    auto parse_dice = [&]() {
        if (token.type == Type::d) {
            eat(Type::d);
            retval.add_dice(eat(Type::number), last_sign == '+' ? 1 : -1);
            return;
        }

        const int i = eat(Type::number);

        if (token.type != Type::d) {
            retval.add_dice(1, last_sign == '+' ? i : -i);
            return;
        }

        eat(Type::d);
        retval.add_dice(eat(Type::number), last_sign == '+' ? i : -i);
    };

    parse_dice();
    while (token.type == Type::plus || token.type == Type::minus) {
        last_sign = token.type == Type::plus ? '+' : '-';
        eat(token.type);
        parse_dice();
    }

    eat(Type::eof);
    return retval;
}

DiceSet& DiceSet::add_dice(int sides, int amount) {
    if (sides <= 0) {
        throw std::invalid_argument { "amount of sides must be positive" };
    }

    m_dice[sides] += amount;
    return *this;
}

DiceRoll DiceSet::roll(Random& random) const {
    DiceRoll retval;

    for (const auto& [sides, amount] : m_dice) {
        for (int i = 0; i < std::abs(amount); ++i) {
            retval.add_dice(sides, random(1, sides) * (amount < 0 ? -1 : 1));
        }
    }

    return retval;
}

std::string DiceSet::to_string() const noexcept {
    std::string retval {};

    if (m_dice.empty()) {
        return {};
    }

    bool first { true };
    for (const auto& [sides, amount] : m_dice) {
        if (amount == 0) {
            continue;
        }

        if (amount < 0) {
            retval += first ? "-" : " - ";
        } else {
            retval += first ? "" : " + ";
        }

        retval += std::to_string(std::abs(amount));

        if (sides != 1) {
            retval += fmt::format("d{}", sides);
        }

        first = false;
    }

    return retval;
}

} /* namespace dnd5e */

namespace YAML {

Node convert<dnd5e::DiceSet>::encode(const dnd5e::DiceSet& rhs) {
    return Node { rhs.to_string() };
}

bool convert<dnd5e::DiceSet>::decode(const Node& node, dnd5e::DiceSet& rhs) {
    try {
        rhs = dnd5e::DiceSet::parse(node.as<std::string>());
    } catch (const std::runtime_error& e) {
        throw RepresentationException {
            node.Mark(),
            "Failed to parse DiceSet: " + std::string { e.what() }
        };
    }
    return true;
}

} /* namespace YAML */

#if ENABLE_TESTS
#include <gtest/gtest.h>

using namespace dnd5e;

class MockRandom : public Random {
public:
    MockRandom(int value) :
        m_value { value } {
    }

    int operator()(int, int) override {
        return m_value;
    }

private:
    int m_value;
};

TEST(Dice, DefaultConstructible) {
    DiceSet diceSet;
    EXPECT_TRUE(diceSet.empty());
}

TEST(Dice, AddDice) {
    auto dice = DiceSet {}.add_dice(6, 10);

    EXPECT_NE(dice, DiceSet {}.add_dice(6));
    EXPECT_NE(dice, DiceSet {}.add_dice(4, 2));
    EXPECT_EQ(dice, DiceSet {}.add_dice(6).add_dice(6, 9));

    auto sorted = DiceSet {}.add_dice(1, 2).add_dice(3, 4).add_dice(5, 6);
    int last_sides = 100;
    for (const auto& [sides, amount] : sorted) {
        EXPECT_LT(sides, last_sides);
        last_sides = sides;
    }
}

TEST(Dice, ToString) {
    EXPECT_EQ("7", DiceSet {}.add_dice(1, 7).to_string());
    EXPECT_EQ("-3", DiceSet {}.add_dice(1, -3).to_string());
    EXPECT_EQ("1d6", DiceSet {}.add_dice(6).to_string());
    EXPECT_EQ("2", DiceSet {}.add_dice(1).add_dice(1).to_string());
    EXPECT_EQ("", DiceSet {}.to_string());
    EXPECT_EQ("", DiceSet {}.add_dice(5, 1).add_dice(5, -1).to_string());

    auto dice = DiceSet {};
    dice.add_dice(1, 2);
    dice.add_dice(3, -4);
    dice.add_dice(5, 6);
    dice.add_dice(7, -8);
    EXPECT_EQ("-8d7 + 6d5 - 4d3 + 2", dice.to_string());
}

TEST(Dice, Parse) {
    EXPECT_EQ("4", DiceSet::parse("4").to_string());
    EXPECT_EQ("1d6", DiceSet::parse("d6").to_string());
    EXPECT_EQ("1d6", DiceSet::parse("1d6").to_string());
    EXPECT_EQ("2d8 - 4", DiceSet::parse("2d8-4d1").to_string());
    EXPECT_EQ("-1d8 + 5d6 + 4", DiceSet::parse("5d6-1d8   +4d1").to_string());
}

TEST(Dice, Roll) {
    MockRandom random { 7 };
    auto expected1 = DiceRoll {}.add_dice(6, 7).add_dice(4, 7).add_dice(4, 7);
    auto roll1 = DiceSet {}.add_dice(6).add_dice(4, 2).roll(random);
    EXPECT_EQ(expected1, roll1);
    EXPECT_EQ("7/6, 7/4, 7/4, total: 21", roll1.to_string());

    auto roll2 = DiceSet {}.add_dice(4, -1).add_dice(6, -2).roll(random);
    for (const auto& [sides, result] : roll2) {
        EXPECT_GT(0, result);
    }

    auto roll3 = DiceSet {}.add_dice(10).add_dice(1).roll(random);
    EXPECT_EQ("7/10, 7/1, total: 14", roll3.to_string());
}

TEST(Dice, IsRandom) {
    auto& random = Random::instance();
    int array[1000];

    for (int& element : array) {
        element = random(0, 1000000);
    }

    EXPECT_TRUE(std::any_of(std::begin(array), std::end(array), [&](int val) {
        return val != array[0];
    }));
}

TEST(Dice, InvalidInput) {
    EXPECT_THROW(DiceSet::parse("1w6"), std::runtime_error);
    EXPECT_THROW(DiceSet::parse("1++2"), std::runtime_error);
    EXPECT_THROW(DiceSet {}.add_dice(-10, 1), std::invalid_argument);
}

#endif /* ENABLE_TESTS */
