// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "database.h"

#include <fmt/core.h>
#include <yaml-cpp/yaml.h>

#define DECODE_REQUIRED(name) \
    do { \
        keys.insert(#name); \
        auto tmp = node[#name]; \
        if (!tmp) { \
            throw KeyNotFound { node.Mark(), std::string { #name } }; \
        } \
        if (!convert<decltype(rhs.name)>::decode(tmp, rhs.name)) { \
            throw BadConversion { tmp.Mark() }; \
        } \
    } while (false)

#define DECODE_OPTIONAL(name) \
    do { \
        keys.insert(#name); \
        auto tmp = node[#name]; \
        if (!tmp) { \
            rhs.name = decltype(rhs.name) {}; \
            break; \
        } \
        if (!convert<decltype(rhs.name)>::decode(tmp, rhs.name)) { \
            throw BadConversion { tmp.Mark() }; \
        } \
    } while (false)

#define ENCODE_REQUIRED(name) \
    do { \
        node[#name] = rhs.name; \
    } while (false)

#define ENCODE_OPTIONAL(name) \
    do { \
        if (isNotEmpty(rhs.name)) { \
            node[#name] = rhs.name; \
        } \
    } while (false)

static void replace_all(
        std::string& s,
        const std::string& pattern,
        const std::string& replacement) {
    size_t i {};

    while (true) {
        i = s.find(pattern, i);
        if (i == std::string::npos) {
            return;
        }

        s.replace(i, pattern.size(), replacement);
        i += replacement.size();
    }
}

static void limit_keys_to(
        const YAML::Node& node,
        const std::set<std::string>& keys) {
    for (const auto& x : node) {
        auto key = x.first.as<std::string>();
        if (keys.find(key) == keys.end()) {
            throw YAML::RepresentationException {
                node.Mark(),
                "Invalid key: " + key
            };
        }
    }
}

static void validate_damage_map(
        const YAML::Node& node,
        const std::map<std::string, std::string>& damage,
        const std::map<std::string, std::string>& variables = {}) {

    std::string attack;
    std::string effect;

    try {
        auto dmg = dnd5e::parse_damage_map(damage, variables, &attack, &effect);
        static_cast<void>(dmg);
    } catch (const std::runtime_error& e) {
        throw YAML::RepresentationException {
            node.Mark(),
            "Failed to parse DiceSet: " + std::string { e.what() }
        };
    }
}

template<typename T>
using empty_t = decltype(std::declval<T>().empty());

template<typename T, typename = std::void_t<>>
struct has_empty : std::false_type {};

template<typename T>
struct has_empty<T, std::void_t<empty_t<T>>> : std::true_type {};

template<typename T>
static bool isNotEmpty(const T& value) {
    if constexpr (std::is_same_v<T, dnd5e::Magic>) {
        return isNotEmpty(value.attribute_name);
    } else if constexpr (has_empty<T>::value) {
        return !value.empty();
    } else {
        return value != T {};
    }
}

namespace YAML {

template<typename T> struct convert<std::set<T>> {
    static Node encode(const std::set<T>& rhs) {
        std::vector<T> tmp { rhs.begin(), rhs.end() };
        return convert<std::vector<T>>::encode(tmp);
    }

    static bool decode(const Node& node, std::set<T>& rhs) {
        std::vector<T> tmp;
        if (!convert<std::vector<T>>::decode(node, tmp)) {
            return false;
        }

        rhs = { tmp.begin(), tmp.end() };
        return true;
    }
};

Node convert<dnd5e::SpellDescription>::encode(
        const dnd5e::SpellDescription& rhs) {
    Node node {};
    ENCODE_REQUIRED(name);
    ENCODE_OPTIONAL(source);
    ENCODE_REQUIRED(level);
    ENCODE_OPTIONAL(ritual);
    ENCODE_REQUIRED(school);
    ENCODE_REQUIRED(time);
    ENCODE_REQUIRED(range);
    ENCODE_REQUIRED(components);
    ENCODE_REQUIRED(duration);
    ENCODE_REQUIRED(description);
    ENCODE_OPTIONAL(roll);
    return node;
}

bool convert<dnd5e::SpellDescription>::decode(
        const Node& node,
        dnd5e::SpellDescription& rhs) {
    if (!node.IsMap()) {
        return false;
    }

    std::set<std::string> keys;
    DECODE_REQUIRED(name);
    DECODE_OPTIONAL(source);
    DECODE_REQUIRED(level);
    DECODE_OPTIONAL(ritual);
    DECODE_REQUIRED(school);
    DECODE_REQUIRED(time);
    DECODE_REQUIRED(range);
    DECODE_REQUIRED(components);
    DECODE_REQUIRED(duration);
    DECODE_REQUIRED(description);
    DECODE_OPTIONAL(roll);
    limit_keys_to(node, keys);

    const std::map<std::string, std::string> variables = {
        { "spell_ability", "1" },
        { "spell_dc", "1" },
        { "spell_attack", "1" }
    };

    for (const auto& [_, val] : rhs.roll) {
        validate_damage_map(node, val, variables);
    }

    return true;
}

Node convert<dnd5e::Magic>::encode(const dnd5e::Magic& rhs) {
    Node node {};
    ENCODE_REQUIRED(attribute_name);
    ENCODE_REQUIRED(attribute_value);
    ENCODE_REQUIRED(difficulty);
    ENCODE_REQUIRED(attack);
    ENCODE_OPTIONAL(spells);
    ENCODE_OPTIONAL(spellbooks);
    ENCODE_OPTIONAL(prepared);
    return node;
}

bool convert<dnd5e::Magic>::decode(const Node& node, dnd5e::Magic& rhs) {
    if (!node.IsMap()) {
        return false;
    }

    std::set<std::string> keys;
    DECODE_REQUIRED(attribute_name);
    DECODE_REQUIRED(attribute_value);
    DECODE_REQUIRED(difficulty);
    DECODE_REQUIRED(attack);
    DECODE_OPTIONAL(spells);
    DECODE_OPTIONAL(spellbooks);
    DECODE_OPTIONAL(prepared);
    limit_keys_to(node, keys);

    for (const auto& spellbook : rhs.spellbooks) {
        rhs.spellbooks_loaded.push_back(
                YAML::LoadFile(spellbook)
                        .as<std::vector<dnd5e::SpellDescription>>(
                                std::vector<dnd5e::SpellDescription> {}));
    }

    return true;
}

Node convert<dnd5e::Action>::encode(const dnd5e::Action& rhs) {
    Node node {};
    ENCODE_REQUIRED(name);
    ENCODE_OPTIONAL(type);
    ENCODE_OPTIONAL(targets);
    ENCODE_OPTIONAL(range);
    ENCODE_OPTIONAL(extra);
    ENCODE_OPTIONAL(bonus);
    ENCODE_OPTIONAL(damage);
    return node;
}

bool convert<dnd5e::Action>::decode(const Node& node, dnd5e::Action& rhs) {
    if (!node.IsMap()) {
        return false;
    }

    std::set<std::string> keys;
    DECODE_REQUIRED(name);
    DECODE_OPTIONAL(type);
    DECODE_OPTIONAL(targets);
    DECODE_OPTIONAL(range);
    DECODE_OPTIONAL(extra);
    DECODE_OPTIONAL(bonus);
    DECODE_OPTIONAL(damage);
    limit_keys_to(node, keys);

    validate_damage_map(node, rhs.damage);

    return true;
}

Node convert<dnd5e::Monster>::encode(const dnd5e::Monster& rhs) {
    Node node {};
    ENCODE_REQUIRED(name);
    ENCODE_REQUIRED(type);
    ENCODE_REQUIRED(attributes);
    ENCODE_OPTIONAL(traits);
    ENCODE_OPTIONAL(actions);
    return node;
}

bool convert<dnd5e::Monster>::decode(const Node& node, dnd5e::Monster& rhs) {
    if (!node.IsMap()) {
        return false;
    }

    std::set<std::string> keys;
    DECODE_REQUIRED(name);
    DECODE_REQUIRED(type);
    DECODE_REQUIRED(attributes);
    DECODE_OPTIONAL(traits);
    DECODE_OPTIONAL(actions);
    limit_keys_to(node, keys);

    return true;
}

Node convert<dnd5e::Feature>::encode(const dnd5e::Feature& rhs) {
    Node node {};
    ENCODE_REQUIRED(name);
    ENCODE_REQUIRED(when);
    ENCODE_OPTIONAL(description);
    ENCODE_OPTIONAL(roll);
    return node;
}

bool convert<dnd5e::Feature>::decode(const Node& node, dnd5e::Feature& rhs) {
    if (!node.IsMap()) {
        return false;
    }

    std::set<std::string> keys;
    DECODE_REQUIRED(name);
    DECODE_OPTIONAL(when);
    DECODE_OPTIONAL(description);
    DECODE_OPTIONAL(roll);
    limit_keys_to(node, keys);

    if (rhs.when.empty()) {
        rhs.when = "permanent";
    }
    return true;
}

Node convert<dnd5e::Equipment>::encode(const dnd5e::Equipment& rhs) {
    Node node {};
    ENCODE_REQUIRED(name);
    ENCODE_OPTIONAL(description);
    ENCODE_OPTIONAL(comment);
    ENCODE_OPTIONAL(worth);
    ENCODE_OPTIONAL(weight);
    ENCODE_OPTIONAL(quantity);
    ENCODE_OPTIONAL(worn);
    return node;
}

bool convert<dnd5e::Equipment>::decode(
        const Node& node,
        dnd5e::Equipment& rhs) {
    if (!node.IsMap()) {
        return false;
    }

    std::set<std::string> keys;
    DECODE_REQUIRED(name);
    DECODE_OPTIONAL(description);
    DECODE_OPTIONAL(comment);
    DECODE_OPTIONAL(worth);
    DECODE_OPTIONAL(weight);
    DECODE_OPTIONAL(quantity);
    DECODE_OPTIONAL(worn);
    limit_keys_to(node, keys);

    return true;
}

Node convert<dnd5e::Character>::encode(const dnd5e::Character& rhs) {
    Node node {};
    ENCODE_REQUIRED(abilities);
    ENCODE_REQUIRED(attributes);
    ENCODE_REQUIRED(skills);
    ENCODE_OPTIONAL(skill_proficiencies);
    ENCODE_REQUIRED(hitpoints);
    ENCODE_REQUIRED(hitdice);
    ENCODE_OPTIONAL(features);
    ENCODE_OPTIONAL(equipment);
    ENCODE_OPTIONAL(money);
    ENCODE_OPTIONAL(magic);
    ENCODE_OPTIONAL(background);
    return node;
}

bool convert<dnd5e::Character>::decode(
        const Node& node,
        dnd5e::Character& rhs) {
    if (!node.IsMap()) {
        return false;
    }

    std::set<std::string> keys;
    DECODE_REQUIRED(abilities);
    DECODE_REQUIRED(attributes);
    DECODE_REQUIRED(skills);
    DECODE_OPTIONAL(skill_proficiencies);
    DECODE_REQUIRED(hitpoints);
    DECODE_REQUIRED(hitdice);
    DECODE_OPTIONAL(features);
    DECODE_OPTIONAL(equipment);
    DECODE_OPTIONAL(money);
    DECODE_OPTIONAL(magic);
    DECODE_OPTIONAL(background);
    limit_keys_to(node, keys);

    return true;
}

} /* namespace YAML */

namespace dnd5e {

std::istream& operator>>(
        std::istream& stream,
        std::vector<Monster>& monsters) {

    YAML::Node node = YAML::Load(stream);

    if (node.IsSequence()) {
        auto m = node.as<std::vector<Monster>>();
        monsters.insert(monsters.end(), m.begin(), m.end());
    } else {
        monsters.push_back(node.as<Monster>());
    }

    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Monster& monster) {
    return stream << YAML::Node { monster };
}

std::ostream& operator<<(
        std::ostream& stream,
        const std::vector<Monster>& monsters) {
    return stream << YAML::Node { monsters };
}

std::istream& operator>>(std::istream& stream, Character& character) {
    YAML::Node node = YAML::Load(stream);
    character = node.as<Character>();

    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Character& character) {
    return stream << YAML::Node { character };
}

std::istream& operator>>(
        std::istream& stream,
        std::vector<SpellDescription>& spells) {

    YAML::Node node = YAML::Load(stream);

    auto s = node.as<std::vector<SpellDescription>>();
    spells.insert(spells.end(), s.begin(), s.end());

    return stream;
}

std::map<std::string, DiceSet> parse_damage_map(
        const std::map<std::string, std::string>& map,
        const std::map<std::string, std::string>& variables,
        std::string* attack,
        std::string* effect) {

    auto apply_variables = [&](const std::string& string) {
        std::string result = string;

        for (const auto& [var_key, var_val] : variables) {
            replace_all(result, fmt::format("${{{}}}", var_key), var_val);
        }

        return result;
    };

    std::map<std::string, DiceSet> result {};

    for (const auto& [key, value] : map) {
        if (attack && key == "attack") {
            *attack = apply_variables(value);
            continue;
        }

        if (effect && key == "effect") {
            *effect = apply_variables(value);
            continue;
        }

        result[key] = DiceSet::parse(apply_variables(value));
    }

    return result;
}

} /* namespace dnd5e */

#if ENABLE_TESTS
#include <gtest/gtest.h>

using namespace dnd5e;

TEST(Database, DiceSet) {
    auto a = DiceSet::parse("4d6 + 1d4 - 2");

    YAML::Node node { a };

    auto b = node.as<DiceSet>();
    EXPECT_EQ(a, b);
}

TEST(Database, SpellDescription) {
    // must not be empty
    auto node1 = YAML::Load("");
    EXPECT_THROW(
            node1.as<SpellDescription>(),
            YAML::TypedBadConversion<SpellDescription>);

    // must be a map
    auto node2 = YAML::Load("[]");
    EXPECT_THROW(
            node2.as<SpellDescription>(),
            YAML::TypedBadConversion<SpellDescription>);

    // must require key "name"
    auto node3 = YAML::Load("bonus: 7");
    EXPECT_THROW(node3.as<SpellDescription>(), YAML::KeyNotFound);

    // must require key "name"
    auto node4 = YAML::Load("type: a\nattributes: {}");
    EXPECT_THROW(node4.as<SpellDescription>(), YAML::KeyNotFound);

    // must require key "type"
    auto node5 = YAML::Load("name: a\nattributes: {}");
    EXPECT_THROW(node5.as<SpellDescription>(), YAML::KeyNotFound);

    // must require key "attributes"
    auto node6 = YAML::Load("name: a\ntype: b");
    EXPECT_THROW(node6.as<SpellDescription>(), YAML::KeyNotFound);

    // must reject invalid keys
    auto string7 =
            "name: a\n"
            "source: b\n"
            "level: 1\n"
            "ritual: true\n"
            "school: c\n"
            "time: d\n"
            "range: e\n"
            "components: f\n"
            "duration: g\n"
            "description: h\n"
            "roll: {}\n"
            "foo: bar";
    auto node7 = YAML::Load(string7);
    EXPECT_THROW(node7.as<SpellDescription>(), YAML::RepresentationException);

    // must not print unused optional keys
    auto string8 =
            "name: a\n"
            "level: 1\n"
            "school: b\n"
            "time: c\n"
            "range: d\n"
            "components: e\n"
            "duration: f\n"
            "description: g";
    auto node8 = YAML::Node { YAML::Load(string8).as<SpellDescription>() };
    EXPECT_EQ(string8, YAML::Dump(node8));

    // round trip test
    auto string9 =
            "name: a\n"
            "source: b\n"
            "level: 1\n"
            "ritual: true\n"
            "school: c\n"
            "time: d\n"
            "range: e\n"
            "components: f\n"
            "duration: g\n"
            "description: h\n"
            "roll:\n"
            "  1:\n"
            "    blunt: 1d6 + 3";
    auto node9 = YAML::Node { YAML::Load(string9).as<SpellDescription>() };
    EXPECT_EQ(string9, YAML::Dump(node9));
}

TEST(Database, SpellDescriptionIO) {
    std::vector<SpellDescription> spells { 2 };

    std::istringstream stream { YAML::Dump(YAML::Node { spells }) };
    EXPECT_TRUE(!!(stream >> spells));
    EXPECT_EQ(4, spells.size());
}

TEST(Database, Action) {
    // must not be empty
    auto node1 = YAML::Load("");
    EXPECT_THROW(node1.as<Action>(), YAML::TypedBadConversion<Action>);

    // must be a map
    auto node2 = YAML::Load("[]");
    EXPECT_THROW(node2.as<Action>(), YAML::TypedBadConversion<Action>);

    // must require key "name"
    auto node3 = YAML::Load("bonus: 7");
    EXPECT_THROW(node3.as<Action>(), YAML::KeyNotFound);

    // must reject invalid keys
    auto node4 = YAML::Load("name: a\nfoo: bar");
    EXPECT_THROW(node4.as<Action>(), YAML::RepresentationException);

    // must not print unused optional keys
    auto string5 = "name: a";
    auto node5 = YAML::Node { YAML::Load(string5).as<Action>() };
    EXPECT_EQ(string5, YAML::Dump(node5));

    // must print used optional keys
    auto string6 = "name: a\nbonus: -7";
    auto node6 = YAML::Node { YAML::Load(string6).as<Action>() };
    EXPECT_EQ(string6, YAML::Dump(node6));

    // round trip test
    auto string7 =
            "name: a\n"
            "type: b\n"
            "targets: c\n"
            "range: d\n"
            "extra: e\n"
            "bonus: 5\n"
            "damage:\n"
            "  type1: 1d2 + 3\n"
            "  type2: 4d5 - 6";
    auto node7 = YAML::Node { YAML::Load(string7).as<Action>() };
    EXPECT_EQ(string7, YAML::Dump(node7));
}

TEST(Database, Monster) {
    // must not be empty
    auto node1 = YAML::Load("");
    EXPECT_THROW(node1.as<Monster>(), YAML::TypedBadConversion<Monster>);

    // must be a map
    auto node2 = YAML::Load("[]");
    EXPECT_THROW(node2.as<Monster>(), YAML::TypedBadConversion<Monster>);

    // must require key "name"
    auto node3 = YAML::Load("bonus: 7");
    EXPECT_THROW(node3.as<Monster>(), YAML::KeyNotFound);

    // must require key "name"
    auto node4 = YAML::Load("type: a\nattributes: {}");
    EXPECT_THROW(node4.as<Monster>(), YAML::KeyNotFound);

    // must require key "type"
    auto node5 = YAML::Load("name: a\nattributes: {}");
    EXPECT_THROW(node5.as<Monster>(), YAML::KeyNotFound);

    // must require key "attributes"
    auto node6 = YAML::Load("name: a\ntype: b");
    EXPECT_THROW(node6.as<Monster>(), YAML::KeyNotFound);

    // must reject invalid keys
    auto node7 = YAML::Load("name: a\ntype: b\nattributes:\n  {}\nfoo: bar");
    EXPECT_THROW(node7.as<Monster>(), YAML::RepresentationException);

    // must not print unused optional keys
    auto string8 = "name: a\ntype: b\nattributes:\n  {}";
    auto node8 = YAML::Node { YAML::Load(string8).as<Monster>() };
    EXPECT_EQ(string8, YAML::Dump(node8));

    // must print used optional keys
    auto string9 =
            "name: a\n"
            "type: b\n"
            "attributes:\n"
            "  {}\n"
            "traits:\n"
            "  foo: bar";
    auto node9 = YAML::Node { YAML::Load(string9).as<Monster>() };
    EXPECT_EQ(string9, YAML::Dump(node9));

    // round trip test
    auto string10 =
            "name: a\n"
            "type: b\n"
            "attributes:\n"
            "  c: 1\n"
            "  d: 2\n"
            "traits:\n"
            "  e: f\n"
            "actions:\n"
            "  - name: g\n"
            "  - name: h\n"
            "    type: i";
    auto node10 = YAML::Node { YAML::Load(string10).as<Monster>() };
    EXPECT_EQ(string10, YAML::Dump(node10));
}

TEST(Database, MonsterIO) {
    std::vector<Monster> monsters { 2 };

    std::stringstream stream1;
    EXPECT_TRUE(!!(stream1 << monsters[0]));
    EXPECT_EQ(stream1.str(), YAML::Dump(YAML::Node { monsters[0] }));

    std::stringstream stream2;
    EXPECT_TRUE(!!(stream2 << monsters));
    EXPECT_EQ(stream2.str(), YAML::Dump(YAML::Node { monsters }));

    std::istringstream stream3 { YAML::Dump(YAML::Node { Monster {} }) };
    EXPECT_TRUE(!!(stream3 >> monsters));
    EXPECT_EQ(3, monsters.size());

    std::istringstream stream4 { YAML::Dump(YAML::Node { monsters }) };
    EXPECT_TRUE(!!(stream4 >> monsters));
    EXPECT_EQ(6, monsters.size());
}

TEST(Database, ParseDamageMap) {
    std::map<std::string, std::string> variables = {
        { "foo", "7" },
        { "bar", "9" }
    };

    std::map<std::string, std::string> damage = {
        { "attack", "${foo} + 1" },
        { "effect", "20 - ${bar}" },
        { "foobar", "${foo}d1 + 3d${bar}" }
    };

    std::map<std::string, DiceSet> diceset;
    std::string attack;
    std::string effect;
    diceset = parse_damage_map(damage, variables, &attack, &effect);

    EXPECT_EQ("7 + 1", attack);
    EXPECT_EQ("20 - 9", effect);
    EXPECT_EQ(1, diceset.size());
    EXPECT_EQ("3d9 + 7", diceset.at("foobar").to_string());
}

#endif /* ENABLE_TESTS */
