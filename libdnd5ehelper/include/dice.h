// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#ifndef DICE_H_
#define DICE_H_

#include <map>
#include <string>

namespace dnd5e {

class Random {
public:
    static Random& instance();

    virtual int operator()(int from, int to) = 0;
    virtual ~Random() noexcept = default;
};

class DiceRoll {
public:
    DiceRoll& add_dice(int sides, int result);

    [[nodiscard]] int sum() const noexcept;

    [[nodiscard]] auto begin() const noexcept {
        return m_dice.begin();
    }

    [[nodiscard]] auto end() const noexcept {
        return m_dice.end();
    }

    [[nodiscard]] std::string to_string() const noexcept;

    [[nodiscard]] bool operator==(const DiceRoll& rhs) const noexcept {
        return m_dice == rhs.m_dice;
    }

    [[nodiscard]] bool operator!=(const DiceRoll& rhs) const noexcept {
        return !(*this == rhs);
    }

private:
    std::multimap<int /* sides */, int /* result */, std::greater<int>> m_dice;
};

class DiceSet {
public:
    // diceset := dice (("+" | "-") dice)*;
    // dice := <int> | "d" <int> | <int> "d" <int>;
    // "5d6 - 1d8 + 4d1", "2d8 + 4d1", "1d6", "4", "d6"
    static DiceSet parse(const std::string&) noexcept(false);
    static DiceSet parse(std::istream&) noexcept(false);

    DiceSet& add_dice(int sides, int amount = 1);

    [[nodiscard]] DiceRoll roll(Random&) const;

    [[nodiscard]] auto begin() const noexcept {
        return m_dice.begin();
    }

    [[nodiscard]] auto end() const noexcept {
        return m_dice.end();
    }

    [[nodiscard]] bool empty() const noexcept {
        return m_dice.empty();
    }

    [[nodiscard]] std::string to_string() const noexcept;

    [[nodiscard]] bool operator==(const DiceSet& rhs) const noexcept {
        return m_dice == rhs.m_dice;
    }

    [[nodiscard]] bool operator!=(const DiceSet& rhs) const noexcept {
        return !(*this == rhs);
    }

private:
    std::map<int /* sides */, int /* amount */, std::greater<int>> m_dice;
};

} /* namespace dnd5e */

namespace YAML {

class Node;

template<typename T>
struct convert;

template<> struct convert<dnd5e::DiceSet> {
    static Node encode(const dnd5e::DiceSet& rhs);
    static bool decode(const Node& node, dnd5e::DiceSet& rhs);
};

} /* namespace YAML */

#endif /* DICE_H_ */
