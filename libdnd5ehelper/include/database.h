// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#ifndef DATABASE_H_
#define DATABASE_H_

#include "dice.h"

#include <map>
#include <set>
#include <string>
#include <vector>

namespace dnd5e {

struct SpellDescription {
    std::string name {};
    std::string source {};
    int level {};
    bool ritual {};
    std::string school {};
    std::string time {};
    std::string range {};
    std::string components {};
    std::string duration {};
    std::string description {};
    std::map<std::string, std::map<std::string, std::string>> roll {};
};

struct Magic {
    std::string attribute_name {};
    int attribute_value {};
    int difficulty {};
    int attack {};
    std::vector<std::string> spellbooks {};
    std::vector<SpellDescription> spells {};
    std::map<std::string, bool> prepared {};
    mutable std::vector<std::vector<SpellDescription>> spellbooks_loaded {};
};

struct Action {
    std::string name {};
    std::string type {};
    std::string targets {};
    std::string range {};
    std::string extra {};
    int bonus {};
    std::map<std::string, std::string> damage {};
};

struct Monster {
    std::string name {};
    std::string type {};
    std::map<std::string, float> attributes {};
    std::map<std::string, std::string> traits {};
    std::vector<Action> actions {};
};

struct Feature {
    std::string name {};
    std::string when {};
    std::string description {};
    std::map<std::string, std::string> roll {};
};

struct Equipment {
    std::string name {};
    std::string description {};
    std::string comment {};
    std::string worth {};
    std::string weight {};
    int quantity {};
    bool worn {};
};

struct Character {
    std::map<std::string, int> abilities {};
    std::map<std::string, std::string> attributes {};
    std::map<std::string, int> skills {};
    std::set<std::string> skill_proficiencies {};
    int hitpoints;
    DiceSet hitdice;
    std::vector<Feature> features {};
    std::vector<Equipment> equipment {};
    unsigned long long money {};
    Magic magic {};
    std::string background {};
};

std::istream& operator>>(std::istream&, std::vector<Monster>&);
std::ostream& operator<<(std::ostream&, const Monster&);
std::ostream& operator<<(std::ostream&, const std::vector<Monster>&);

std::istream& operator>>(std::istream&, Character&);
std::ostream& operator<<(std::ostream&, const Character&);

std::istream& operator>>(std::istream&, std::vector<SpellDescription>&);

std::map<std::string, DiceSet> parse_damage_map(
        const std::map<std::string, std::string>& map,
        const std::map<std::string, std::string>& variables,
        std::string* attack = nullptr,
        std::string* effect = nullptr);

} /* namespace dnd5e */

namespace YAML {

class Node;

template<typename T>
struct convert;

template<> struct convert<dnd5e::SpellDescription> {
    static Node encode(const dnd5e::SpellDescription& rhs);
    static bool decode(const Node& node, dnd5e::SpellDescription& rhs);
};

template<> struct convert<dnd5e::Magic> {
    static Node encode(const dnd5e::Magic& rhs);
    static bool decode(const Node& node, dnd5e::Magic& rhs);
};

template<> struct convert<dnd5e::Action> {
    static Node encode(const dnd5e::Action& rhs);
    static bool decode(const Node& node, dnd5e::Action& rhs);
};

template<> struct convert<dnd5e::Monster> {
    static Node encode(const dnd5e::Monster& rhs);
    static bool decode(const Node& node, dnd5e::Monster& rhs);
};

template<> struct convert<dnd5e::Feature> {
    static Node encode(const dnd5e::Feature& rhs);
    static bool decode(const Node& node, dnd5e::Feature& rhs);
};

template<> struct convert<dnd5e::Equipment> {
    static Node encode(const dnd5e::Equipment& rhs);
    static bool decode(const Node& node, dnd5e::Equipment& rhs);
};

template<> struct convert<dnd5e::Character> {
    static Node encode(const dnd5e::Character& rhs);
    static bool decode(const Node& node, dnd5e::Character& rhs);
};

} /* namespace YAML */

#endif /* DATABASE_H_ */
