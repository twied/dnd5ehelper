# Size chart

| Size       | Space                 | Squares    | Examples            |
| ---------- | --------------------- | ---------- | ------------------- |
| Tiny       | 2.5 by 2.5 ft         | 0.5 by 0.5 | Imp, Sprite         |
| Small      | 5 by 5 ft             | 1 by 1     | ...                 |
| Medium     | ...                   | ...        | ...                 |
| ...        | ...                   | ...        | ...                 |

---

# Conditions

**Blinded**
* A blinded creature can't see and automatically fails any ability check that requires sight.
* ...

**Charmed**
* ...

...

---

# Spells

## Wizard

**Magic Missle**: 1st level; 1 action; 120 feet; V,S; Instantaneous: Three darts for 1d4+1 force damage each, one creature or several. +1 dart for each level above 1st.

## Cleric

...

---

# Combat

**Determine surprise**: Stealth vs. Perception check. Surprised creatures miss first turn.

**Initiative**: 1d20 + DEX, determines turn order. Each Turn ≈ 6 seconds.

## Turns

In any order:

**Movement**: May be broken up

**Activity**: ...

**Action**: ...

**Bonus Action**: ..

...

---

# Formatting Examples

Here is some text. This is *once* emphacised, **twice** emphacised, **thrice** emphacised. This is ~~scratched~~.

> This is a block quote.

* this
* is
* a
* list
  * with a subitem

Image: ![image title](image.png)

Emoji: 💀 ⚔️ 🐉

This is `monospaced text` in a sentence.

```
 This
          is
    an
    entire
          monospaced block.
```

