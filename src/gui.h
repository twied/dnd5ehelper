// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#ifndef GUI_H_
#define GUI_H_

#include <QFrame>
#include <QGroupBox>
#include <QMainWindow>
#include <QSplitter>
#include <QTextEdit>

namespace dnd5e {

struct Action;
struct Character;
class DiceSet;
struct Feature;
struct SpellDescription;
struct Monster;
class QApplication;
class QTabWidget;

} /* namespace dnd5e */

class HLineWidget : public QFrame {
    Q_OBJECT

public:
    HLineWidget(QWidget* parent = nullptr) :
        QFrame { parent } {
        setFrameShape(QFrame::HLine);
        setFrameShadow(QFrame::Sunken);
    }
};

class BattleLogWidget : public QWidget {
    Q_OBJECT

public:
    BattleLogWidget(QWidget* parent = nullptr);

    void append(const QString& text);
    void roll_manual(const dnd5e::DiceSet& diceset);
    void roll_button(int sides);
    void roll_attribute(const QString& name, int bonus);
    void roll_attack(
            const QString& verb,
            const QString& name,
            int attack_bonus,
            const std::map<std::string, dnd5e::DiceSet>& damagemap,
            const std::string& effect = "");
    void roll_attack(
            const QString& verb,
            const QString& name,
            const std::map<std::string, std::string>& damage,
            const std::map<std::string, std::string>& variables = {});

private:
    QTextEdit* m_textedit;
    QWidget* create_buttons_widget();
    QWidget* create_roll_manual_widget();
};

class MonsterWidget : public QGroupBox {
    Q_OBJECT

public:
    MonsterWidget(
            const dnd5e::Monster& monster,
            BattleLogWidget* log,
            QWidget* parent = nullptr);

private:
    QWidget* create_attributes_widget(const dnd5e::Monster&, BattleLogWidget*);
    QWidget* create_traits_widget(const dnd5e::Monster&);
    QWidget* create_actions_widget(const dnd5e::Monster&, BattleLogWidget*);
};

struct Tab {
    char type;
    QString filename;
};

class CheatSheetTab : public QTextEdit, public Tab {
    Q_OBJECT

public:
    CheatSheetTab(const QString& filename, QWidget* parent = nullptr);
};

class NotesTab : public QTextEdit, public Tab {
    Q_OBJECT

public:
    NotesTab(const QString& filename, QWidget* parent = nullptr);
};

class MonsterBookTab : public QSplitter, public Tab {
    Q_OBJECT

public:
    MonsterBookTab(const QString& filename, QWidget* parent = nullptr);
};

class SpellBookTab : public QSplitter, public Tab {
    Q_OBJECT

public:
    SpellBookTab(const QString& filename, QWidget* parent = nullptr);

private:
    std::map<std::string, std::string> variables {};
    QWidget* create_spell_widget(
            const dnd5e::SpellDescription&,
            BattleLogWidget*);
    QWidget* create_variables_widget();
};

class CharacterTab : public QSplitter, public Tab {
    Q_OBJECT

public:
    CharacterTab(const QString& filename, QWidget* parent = nullptr);
    ~CharacterTab() noexcept;

private:
    std::unique_ptr<dnd5e::Character> m_character;

    QWidget* create_abilities_widget(BattleLogWidget* log);
    QWidget* create_attributes_widget();
    QWidget* create_status_widget();
    QWidget* create_feature_widget(
            BattleLogWidget* log,
            const dnd5e::Feature& feature);
    QWidget* create_spell_widget(
            const dnd5e::SpellDescription& spell,
            BattleLogWidget* log);
    QWidget* create_features_tab(BattleLogWidget* log);
    QWidget* create_equipment_tab();
    QWidget* create_magic_tab(BattleLogWidget* log);
    QWidget* create_background_tab();
};

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QApplication* app);
    void openTab(char type, const QString& path, bool update = true);
    void removeTab(int index);
    void saveTabs();
    void setTheme(QApplication* app, bool dark);

private:
    QTabWidget* tabs;
};

#endif /* GUI_H_ */
