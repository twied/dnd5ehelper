// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "gui.h"

#include "database.h"
#include "dice.h"
#include "dnd5ehelper.h"

#include <QApplication>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QGridLayout>
#include <QKeySequence>
#include <QLabel>
#include <QLineEdit>
#include <QMenuBar>
#include <QPushButton>
#include <QScrollArea>
#include <QSettings>
#include <QShortcut>
#include <QSpinBox>
#include <QStandardPaths>
#include <QTabBar>
#include <QTabWidget>
#include <QTextStream>
#include <QTime>

#include <fstream>

BattleLogWidget::BattleLogWidget(QWidget* parent) :
    QWidget { parent } {
    auto layout = new QVBoxLayout {};
    setLayout(layout);

    layout->setMargin(0);
    layout->addWidget(m_textedit = new QTextEdit {});
    layout->addWidget(create_buttons_widget());
    layout->addWidget(create_roll_manual_widget());
    layout->addWidget(new QWidget {});
}

void BattleLogWidget::append(const QString& text) {
    const auto time = QTime::currentTime().toString("\n[hh:mm:ss] ");
    m_textedit->append(time + text);
}

void BattleLogWidget::roll_manual(const dnd5e::DiceSet& diceset) {
    const auto result = diceset.roll(dnd5e::Random::instance());
    append(tr("rolling \"%1\": %2")
                   .arg(diceset.to_string().c_str())
                   .arg(result.to_string().c_str()));
}

void BattleLogWidget::roll_button(int sides) {
    const auto result = dnd5e::Random::instance()(1, sides);
    append(tr("rolling button 1d%1: %2").arg(sides).arg(result));
}

void BattleLogWidget::roll_attribute(const QString& name, int bonus) {
    const auto result = dnd5e::Random::instance()(1, 20);
    append(tr("rolling %1 : %2/20 %3 %4 = %5")
                   .arg(name)
                   .arg(result)
                   .arg(bonus < 0 ? '-' : '+')
                   .arg(std::abs(bonus))
                   .arg(result + bonus));
}

void BattleLogWidget::roll_attack(
        const QString& verb,
        const QString& name,
        int attack_bonus,
        const std::map<std::string, dnd5e::DiceSet>& damagemap,
        const std::string& effect) {

    auto attack = dnd5e::Random::instance()(1, 20);

    auto text = tr("%1 %2: %3/20 %4 %5 = %6")
                        .arg(verb)
                        .arg(name)
                        .arg(attack)
                        .arg(attack_bonus < 0 ? '-' : '+')
                        .arg(std::abs(attack_bonus))
                        .arg(attack + attack_bonus);

    if (attack == 1 || attack == 20) {
        text += ' ';
        if (attack == 1) {
            text += tr("FUMBLE");
        } else {
            text += tr("CRITICAL");
        }
    }

    for (const auto& [type, dices] : damagemap) {
        auto damage = dices;

        if (attack == 1) {
            dnd5e::DiceSet fumble {};
            for (const auto& [sides, amount] : damage) {
                fumble.add_dice(sides, -amount);
            }
            std::swap(damage, fumble);
        } else if (attack == 20) {
            dnd5e::DiceSet critical {};
            for (const auto& [sides, amount] : damage) {
                critical.add_dice(
                        sides,
                        amount * (sides == 1 ? 1 : 2));
            }
            std::swap(damage, critical);
        }

        auto roll = damage.roll(dnd5e::Random::instance());

        text += tr("\n    %1: %2").arg(type.c_str()).arg(roll.to_string().c_str());
    }

    if (!effect.empty()) {
        text += tr("\n    Effect: %1").arg(effect.c_str());
    }

    append(text);
}

void BattleLogWidget::roll_attack(
        const QString& verb,
        const QString& name,
        const std::map<std::string, std::string>& damage,
        const std::map<std::string, std::string>& variables) {

    std::string attack {};
    std::string effect {};
    auto diceset = dnd5e::parse_damage_map(damage, variables, &attack, &effect);

    if (!attack.empty()) {
        roll_attack(verb, name, std::stoi(attack), diceset, effect);
        return;
    }

    auto text = tr("%1 %2").arg(verb).arg(name);

    if (!diceset.empty()) {
        text += ':';
    }

    for (const auto& [type, dices] : diceset) {
        auto roll = dices.roll(dnd5e::Random::instance());
        text += tr("\n    %1: %2").arg(type.c_str()).arg(roll.to_string().c_str());
    }

    if (!effect.empty()) {
        text += tr("\n    Effect: %1").arg(effect.c_str());
    }

    append(text);
}

QWidget* BattleLogWidget::create_buttons_widget() {
    auto layout = new QGridLayout {};
    layout->setMargin(0);

    constexpr int sides[] = { 2, 4, 6, 8, 10, 12, 20, 100 };
    for (size_t i = 0; i < sizeof(sides) / sizeof(*sides); ++i) {
        auto title = tr("1d%1").arg(sides[i]);

        auto button = new QPushButton { title };
        QObject::connect(button, &QPushButton::clicked, [=] {
            roll_button(sides[i]);
        });

        layout->addWidget(button, i / 4, i % 4);
    }

    auto widget = new QWidget {};
    widget->setLayout(layout);

    return widget;
}

QWidget* BattleLogWidget::create_roll_manual_widget() {
    auto widget = new QLineEdit {};
    QObject::connect(widget, &QLineEdit::returnPressed, [=] {
        auto trimmed = widget->text().trimmed();
        if (trimmed.isEmpty()) {
            return;
        }

        dnd5e::DiceSet dices;
        try {
            dices = dnd5e::DiceSet::parse(trimmed.toStdString());
        } catch (const std::runtime_error& e) {
            append(tr("Error: \"%1\"").arg(e.what()));
            return;
        }

        roll_manual(dices);
        widget->clear();
    });

    return widget;
}

MonsterWidget::MonsterWidget(
        const dnd5e::Monster& monster,
        BattleLogWidget* log,
        QWidget* parent) :
    QGroupBox { parent } {

    if (!monster.type.empty()) {
        setTitle(tr("%1 / %2").arg(monster.name.c_str()).arg(monster.type.c_str()));
    } else {
        setTitle(monster.name.c_str());
    }

    auto layout = new QVBoxLayout {};
    setLayout(layout);

    if (!monster.attributes.empty()) {
        layout->addWidget(create_attributes_widget(monster, log));
    }

    if (!monster.traits.empty()) {
        layout->addWidget(new HLineWidget {});
        layout->addWidget(create_traits_widget(monster));
    }

    if (!monster.actions.empty()) {
        layout->addWidget(new HLineWidget {});
        layout->addWidget(create_actions_widget(monster, log));
    }
}

QWidget* MonsterWidget::create_attributes_widget(
        const dnd5e::Monster& monster,
        BattleLogWidget* log) {

    auto attributes_layout = new QGridLayout {};
    attributes_layout->setMargin(0);

    auto add_ability = [&](int column, const char* name) {
        auto value = 0.0f;
        if (auto it = monster.attributes.find(name);
            it != monster.attributes.end()) {
            value = it->second;
        }

        auto bonus = (static_cast<int>(value) - 10) / 2;

        auto title_widget = new QPushButton { QString { name }.toUpper() };
        title_widget->setMinimumWidth(1);

        QObject::connect(title_widget, &QPushButton::clicked, [=] {
            log->roll_attribute(tr("%1 %2").arg(monster.name.c_str()).arg(name), bonus);
        });

        auto value_widget = new QLabel {};
        value_widget->setAlignment(Qt::AlignCenter);
        value_widget->setNum(value);

        QFont value_font = value_widget->font();
        value_font.setBold(true);
        value_widget->setFont(value_font);

        auto bonus_widget = new QLabel {
            tr("%1%2")
                    .arg(bonus < 0 ? '-' : '+')
                    .arg(std::abs(bonus))
        };
        bonus_widget->setAlignment(Qt::AlignCenter);

        attributes_layout->addWidget(title_widget, 0, column);
        attributes_layout->addWidget(value_widget, 1, column);
        attributes_layout->addWidget(bonus_widget, 2, column);
    };

    auto add_attribute = [&](int column, const char* name) {
        auto value = 0.0f;
        if (auto it = monster.attributes.find(name);
            it != monster.attributes.end()) {
            value = it->second;
        }

        auto title_widget = new QLabel { QString { name }.toUpper() };
        title_widget->setAlignment(Qt::AlignCenter);

        auto value_widget = new QLabel {};
        value_widget->setAlignment(Qt::AlignCenter);
        value_widget->setNum(value);

        QFont value_font = value_widget->font();
        value_font.setBold(true);
        value_widget->setFont(value_font);

        attributes_layout->addWidget(title_widget, 0, column);
        attributes_layout->addWidget(value_widget, 1, column);
    };

    add_ability(0, "str");
    add_ability(1, "dex");
    add_ability(2, "con");
    add_ability(3, "int");
    add_ability(4, "wis");
    add_ability(5, "cha");
    add_attribute(6, "ac");
    add_attribute(7, "hp");
    add_attribute(8, "cr");

    auto widget = new QWidget {};
    widget->setLayout(attributes_layout);
    return widget;
}

QWidget* MonsterWidget::create_traits_widget(const dnd5e::Monster& monster) {
    auto traits_layout = new QGridLayout {};
    traits_layout->setMargin(0);

    size_t index = 0;
    for (const auto& [key, value] : monster.traits) {
        auto key_widget = new QLabel { QString { "%1:" }.arg(key.c_str()) };
        key_widget->setAlignment(Qt::AlignLeft);
        key_widget->setFixedWidth(150);

        auto value_widget = new QLabel { value.c_str() };
        value_widget->setAlignment(Qt::AlignLeft);
        value_widget->setWordWrap(true);

        traits_layout->addWidget(key_widget, index, 0);
        traits_layout->addWidget(value_widget, index, 1);
        index += 1;
    }

    auto widget = new QWidget {};
    widget->setLayout(traits_layout);
    return widget;
}

QWidget* MonsterWidget::create_actions_widget(
        const dnd5e::Monster& monster,
        BattleLogWidget* log) {

    auto actions_layout = new QGridLayout {};
    actions_layout->setMargin(0);

    for (size_t i = 0; i < monster.actions.size(); ++i) {
        const auto& action = monster.actions[i];

        auto title_label = new QLabel { action.name.c_str() };
        if (action.targets.empty()) {
            title_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
            title_label->setFixedWidth(150);
            title_label->setWordWrap(true);

            actions_layout->addWidget(title_label, i, 0);
            actions_layout->setAlignment(
                    title_label,
                    Qt::AlignLeft | Qt::AlignTop);
        } else {
            title_label->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
            title_label->setFixedWidth(150 - 2 * layout()->spacing());
            title_label->setWordWrap(true);
            title_label->adjustSize();
            title_label->setMouseTracking(false);
            title_label->setTextInteractionFlags(Qt::NoTextInteraction);

            auto title_layout = new QHBoxLayout {};
            title_layout->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
            title_layout->addWidget(title_label);
            title_layout->setContentsMargins(
                    layout()->spacing(),
                    0,
                    layout()->spacing(),
                    0);

            auto title_button = new QPushButton {};
            title_button->setLayout(title_layout);
            title_button->adjustSize();
            title_button->setFixedWidth(150);
            title_button->setFixedHeight(std::max(
                    title_button->height(),
                    title_label->height() + layout()->spacing()));

            QObject::connect(title_button, &QPushButton::clicked, [=] {
                log->roll_attack(
                        tr("rolling"),
                        (monster.name + ' ' + action.name).c_str(),
                        action.bonus,
                        dnd5e::parse_damage_map(action.damage, {}),
                        action.extra);
            });

            actions_layout->addWidget(title_button, i, 0);
            actions_layout->setAlignment(
                    title_button,
                    Qt::AlignLeft | Qt::AlignTop);
        }

        QString description_text;
        if (action.targets.empty()) {
            description_text = action.extra.c_str();
        } else {
            description_text = tr("%1 %2%3, targets: %4, reach: %5.")
                                       .arg(action.type.c_str())
                                       .arg(action.bonus < 0 ? '-' : '+')
                                       .arg(std::abs(action.bonus))
                                       .arg(action.targets.c_str())
                                       .arg(action.range.c_str());

            if (!action.damage.empty()) {
                description_text += ' ' + tr("Damage:") + ' ';
                auto first = true;
                for (const auto& [type, dices] : action.damage) {
                    if (!first) {
                        description_text += ", ";
                    } else {
                        first = false;
                    }

                    description_text += QString { "%1 %2" }.arg(dices.c_str()).arg(type.c_str());
                }
                description_text += '.';
            }

            if (!action.extra.empty()) {
                description_text += QString { " %1. " }.arg(action.extra.c_str());
            }
        }

        auto description_widget = new QLabel { description_text };
        description_widget->setSizePolicy(
                QSizePolicy::Expanding,
                QSizePolicy::Expanding);
        description_widget->setWordWrap(true);
        description_widget->setAlignment(Qt::AlignTop | Qt::AlignLeft);
        actions_layout->addWidget(description_widget, i, 1);
    }

    auto widget = new QWidget {};
    widget->setLayout(actions_layout);
    return widget;
}

CheatSheetTab::CheatSheetTab(
        const QString& filename,
        QWidget* parent) :
    QTextEdit { parent },
    Tab { 's', filename } {

    setReadOnly(true);

    if (filename.isEmpty()) {
        setPlainText("Error: Empty filename");
        return;
    }

    QFile file { filename };
    if (!file.open(QIODevice::ReadOnly)) {
        setPlainText("Error: Cannot read " + filename);
        return;
    }

    const QString content = QTextStream { &file }.readAll();
    if (filename.endsWith(".md")) {
        setMarkdown(content);
    } else if (filename.endsWith(".html")) {
        setHtml(content);
    } else {
        setPlainText(content);
    }
}

NotesTab::NotesTab(
        const QString& filename,
        QWidget* parent) :
    QTextEdit { parent },
    Tab { 'n', filename } {

    QFileInfo { filename }.absoluteDir().mkdir(".");

    if (QFile file { filename }; file.open(QIODevice::ReadOnly)) {
        setPlainText(QTextStream { &file }.readAll());
    }

    QObject::connect(this, &QTextEdit::textChanged, [=] {
        if (QFile file { filename }; file.open(QIODevice::WriteOnly)) {
            file.write(toPlainText().toUtf8());
        }
    });
}

MonsterBookTab::MonsterBookTab(
        const QString& filename,
        QWidget* parent) :
    QSplitter { parent },
    Tab { 'm', filename } {

    auto battlelog = new BattleLogWidget {};

    std::vector<dnd5e::Monster> monsters;
    try {
        std::ifstream { filename.toStdString() } >> monsters;

        if (monsters.empty()) {
            battlelog->append("Error: " + filename + " is emtpy.");
        }
    } catch (const std::runtime_error& e) {
        monsters.clear();
        battlelog->append("Error: " + filename + ": " + e.what());
    }

    auto layout = new QVBoxLayout {};
    for (const auto& monster : monsters) {
        layout->addWidget(new MonsterWidget { monster, battlelog });
    }
    layout->addStretch(1);

    auto widget = new QWidget {};
    widget->setLayout(layout);

    auto scroll = new QScrollArea {};
    scroll->setWidget(widget);
    scroll->adjustSize();
    scroll->setMinimumWidth(scroll->width() + 2 * layout->margin());
    scroll->setWidgetResizable(true);

    addWidget(battlelog);
    addWidget(scroll);
}

SpellBookTab::SpellBookTab(
        const QString& filename,
        QWidget* parent) :
    QSplitter { parent },
    Tab { 'b', filename } {

    auto battlelog = new BattleLogWidget {};

    std::vector<dnd5e::SpellDescription> spells;
    try {
        std::ifstream { filename.toStdString() } >> spells;

        if (spells.empty()) {
            battlelog->append("Error: " + filename + " is invalid or emtpy.");
        }
    } catch (const std::runtime_error& e) {
        spells.clear();
        battlelog->append("Error: " + filename + ": " + e.what());
    }

    auto left_layout = new QVBoxLayout {};
    left_layout->setMargin(0);
    left_layout->addWidget(create_variables_widget());
    left_layout->addWidget(battlelog);

    auto left_widget = new QWidget {};
    left_widget->setLayout(left_layout);

    auto right_layout = new QVBoxLayout {};
    for (const auto& spell : spells) {
        right_layout->addWidget(create_spell_widget(spell, battlelog));
    }

    right_layout->addStretch(1);

    auto right_widget = new QWidget {};
    right_widget->setLayout(right_layout);

    auto scroll = new QScrollArea {};
    scroll->setWidget(right_widget);
    scroll->adjustSize();
    scroll->setMinimumWidth(scroll->width() + 2 * right_layout->margin());
    scroll->setWidgetResizable(true);

    addWidget(left_widget);
    addWidget(scroll);
}

QWidget* SpellBookTab::create_spell_widget(
        const dnd5e::SpellDescription& spell,
        BattleLogWidget* log) {

    auto layout = new QVBoxLayout {};

    auto add_button = [=](int level, const std::map<std::string, std::string>& effect) {
        QString tooltip {};
        for (const auto& [key, value] : effect) {
            tooltip += QString { "• <b>%1</b>: %2<br/>" }.arg(key.c_str()).arg(value.c_str());
        }

        if (tooltip.isEmpty()) {
            tooltip = tr("Nothing to roll");
        }

        auto button = new QPushButton { QString::number(level) };
        button->adjustSize();
        button->setFixedWidth(button->height());
        button->setToolTip(tooltip);

        QObject::connect(button, &QPushButton::clicked, [=]() {
            log->roll_attack(
                    tr("casting"),
                    QString { "%1° %2" }.arg(level).arg(spell.name.c_str()),
                    effect,
                    variables);
        });

        return button;
    };

    auto head_label = new QLabel {
        tr("<b>%1</b>: ⛤&nbsp;%2°&nbsp;%3, ⚒︎&nbsp;%4, ↷&nbsp;%5, ⏱&nbsp;%6")
                .arg(spell.name.c_str())
                .arg(spell.level)
                .arg(spell.school.c_str())
                .arg(spell.time.c_str())
                .arg(spell.range.c_str())
                .arg(spell.duration.c_str())
    };
    head_label->setWordWrap(true);
    layout->addWidget(head_label);

    auto components_label = new QLabel {
        tr("Components: %1").arg(spell.components.c_str())
    };
    components_label->setWordWrap(true);
    layout->addWidget(components_label);

    if (!spell.source.empty()) {
        auto source_label = new QLabel { tr("Source: %1").arg(spell.source.c_str()) };
        source_label->setWordWrap(true);
        layout->addWidget(source_label);
    }

    auto description_label = new QLabel { spell.description.c_str() };
    description_label->setWordWrap(true);
    layout->addWidget(description_label);

    auto roll_layout = new QHBoxLayout {};
    roll_layout->setMargin(0);

    roll_layout->addWidget(new QLabel { tr("Cast at level:") });

    for (const auto& [level, effect] : spell.roll) {
        roll_layout->addWidget(add_button(std::stoi(level), effect));
    }

    if (spell.roll.empty()) {
        if (spell.level == 0) {
            roll_layout->addWidget(add_button(spell.level, {}));
        } else {
            for (int level = spell.level; level < 10; ++level) {
                roll_layout->addWidget(add_button(level, {}));
            }
        }
    }

    roll_layout->addStretch(1);

    auto roll_widget = new QWidget {};
    roll_widget->setLayout(roll_layout);
    layout->addWidget(roll_widget);

    auto widget = new QWidget {};
    widget->setLayout(layout);
    return widget;
}

QWidget* SpellBookTab::create_variables_widget() {
    auto layout = new QGridLayout {};

    auto add_variable = [&](int row, const QString& title, const std::string& var) {
        variables[var] = "0";

        auto label = new QLabel { title };
        layout->addWidget(label, row, 0);

        auto spinbox = new QSpinBox {};
        spinbox->setAlignment(Qt::AlignRight);
        spinbox->setPrefix(QString { "${%1} = " }.arg(var.c_str()));
        spinbox->setRange(-10, 30);
        layout->addWidget(spinbox, row, 1);

        QObject::connect(spinbox, &QSpinBox::textChanged, [=] {
            variables[var] = std::to_string(spinbox->value());
        });
    };

    add_variable(0, tr("Spellcasting Ability Modifier"), "spell_ability");
    add_variable(1, tr("Spell Save DC"), "spell_dc");
    add_variable(2, tr("Spell Attack Bonus"), "spell_attack");

    auto widget = new QWidget {};
    widget->setLayout(layout);
    return widget;
}

CharacterTab::CharacterTab(
        const QString& filename,
        QWidget* parent) :
    QSplitter { parent },
    Tab { 'c', filename } {

    auto log = new BattleLogWidget {};

    m_character = std::make_unique<dnd5e::Character>();
    try {
        std::ifstream { filename.toStdString() } >> *m_character;
    } catch (const std::runtime_error& e) {
        log->append("Error: " + filename + ": " + e.what());
    }

    auto left_layout = new QVBoxLayout {};
    left_layout->setMargin(0);
    left_layout->addWidget(create_attributes_widget());
    left_layout->addWidget(new HLineWidget {});
    left_layout->addWidget(create_abilities_widget(log));
    left_layout->addWidget(log);

    auto left_widget = new QWidget {};
    left_widget->setLayout(left_layout);

    auto tab_widget = new QTabWidget {};
    tab_widget->addTab(create_features_tab(log), tr("Features"));
    if (!m_character->equipment.empty()) {
        tab_widget->addTab(create_equipment_tab(), tr("Equipment"));
    }
    if (!m_character->magic.attribute_name.empty()) {
        tab_widget->addTab(create_magic_tab(log), tr("Magic"));
    }
    if (!m_character->background.empty()) {
        tab_widget->addTab(create_background_tab(), tr("Background"));
    }

    auto right_layout = new QVBoxLayout {};
    right_layout->setMargin(0);
    right_layout->addWidget(create_status_widget());
    right_layout->addWidget(tab_widget);

    auto right_widget = new QWidget {};
    right_widget->setLayout(right_layout);

    addWidget(left_widget);
    addWidget(right_widget);
}

CharacterTab::~CharacterTab() noexcept {
    // Need to have an explicit destructor for moc to deal with forward
    // declaration of "struct Character".
}

QWidget* CharacterTab::create_abilities_widget(BattleLogWidget* log) {
    QSettings settings { "dnd5ehelper" };

    int font_width = settings.value("fontwidth").toInt();
    if (font_width == 0) {
        font_width = 100;
    }

    auto layout = new QGridLayout {};
    layout->setMargin(0);

    auto add_ability = [&](int index, const char* name, const QString& title) {
        int value = 0;

        auto it = m_character->abilities.find(name);
        if (it != m_character->abilities.end()) {
            value = it->second;
        }

        auto label = new QLabel {};
        label->setNum(value);
        label->setAlignment(Qt::AlignCenter);
        label->setToolTip(title);

        auto font = label->font();
        font.setBold(true);
        font.setPointSize(font.pointSize() * 2);
        label->setFont(font);

        layout->addWidget(label, 0, index);
    };

    auto add_skill = [&](int x, int y, const char* name, const QString& title) {
        int value = 0;

        auto it = m_character->skills.find(name);
        if (it != m_character->skills.end()) {
            value = it->second;
        }

        auto proficient =
                m_character->skill_proficiencies.find(name)
                != m_character->skill_proficiencies.end();

        auto button_label = new QLabel {
            QString { "%1%2%3: <b>%4%5</b>" }
                    .arg(proficient ? "<u>" : "")
                    .arg(title)
                    .arg(proficient ? "</u>" : "")
                    .arg(value < 0 ? '-' : '+')
                    .arg(std::abs(value))
        };

        auto font = button_label->font();
        font.setStretch(font_width);
        button_label->setFont(font);

        button_label->setAlignment(Qt::AlignLeft);
        button_label->setMouseTracking(false);
        button_label->setTextInteractionFlags(Qt::NoTextInteraction);
        button_label->adjustSize();

        auto button_layout = new QHBoxLayout {};
        button_layout->setContentsMargins(5, 0, 5, 0);
        button_layout->addWidget(button_label);
        button_layout->setAlignment(Qt::AlignBaseline);

        auto button = new QPushButton {};
        button->setLayout(button_layout);
        button->adjustSize();
        button->setMinimumWidth(button->width());

        QObject::connect(button, &QPushButton::clicked, [=] {
            log->roll_attribute(title, value);
        });

        layout->addWidget(button, x, y);
    };

    add_ability(0, "str", tr("Strength"));
    add_ability(1, "dex", tr("Dexterity"));
    add_ability(2, "con", tr("Constitution"));
    add_ability(3, "int", tr("Intelligence"));
    add_ability(4, "wis", tr("Wisdom"));
    add_ability(5, "cha", tr("Charisma"));

    add_skill(1, 0, "str", tr("Strength"));
    add_skill(2, 0, "strsave", tr("STR Save"));
    add_skill(3, 0, "athletics", tr("Athletics"));

    add_skill(1, 1, "dex", tr("Dexterity"));
    add_skill(2, 1, "dexsave", tr("DEX Save"));
    add_skill(3, 1, "acrobatics", tr("Acrobatics"));
    add_skill(4, 1, "initiative", tr("Initiative"));
    add_skill(5, 1, "sleightofhand", tr("Sleight of Hand"));
    add_skill(6, 1, "stealth", tr("Stealth"));

    add_skill(1, 2, "con", tr("Constitution"));
    add_skill(2, 2, "consave", tr("CON Save"));

    add_skill(1, 3, "int", tr("Intelligence"));
    add_skill(2, 3, "intsave", tr("Int Save"));
    add_skill(3, 3, "arcana", tr("Arcana"));
    add_skill(4, 3, "history", tr("History"));
    add_skill(5, 3, "investigation", tr("Investigation"));
    add_skill(6, 3, "nature", tr("Nature"));
    add_skill(7, 3, "religion", tr("Religion"));

    add_skill(1, 4, "wis", tr("Wisdom"));
    add_skill(2, 4, "wissave", tr("WIS Save"));
    add_skill(3, 4, "animalhandling", tr("Animal Handling"));
    add_skill(4, 4, "insight", tr("Insight"));
    add_skill(5, 4, "medicine", tr("Medicine"));
    add_skill(6, 4, "perception", tr("Perception"));
    add_skill(7, 4, "survival", tr("Survival"));

    add_skill(1, 5, "cha", tr("Charisma"));
    add_skill(2, 5, "chasave", tr("CHA Save"));
    add_skill(3, 5, "deception", tr("Deception"));
    add_skill(4, 5, "intimidation", tr("Intimidation"));
    add_skill(5, 5, "performance", tr("Performance"));
    add_skill(6, 5, "persuasion", tr("Persuasion"));

    auto widget = new QWidget {};
    widget->setLayout(layout);
    return widget;
}

QWidget* CharacterTab::create_attributes_widget() {
    auto layout = new QGridLayout {};
    layout->setMargin(0);

    auto add_attribute = [&](int i, const char* name, const QString& title) {
        auto it = m_character->attributes.find(name);
        if (it == m_character->attributes.end()) {
            return;
        }

        auto text = QString { "%1: <b>%2</b>" }.arg(title).arg(it->second.c_str());
        layout->addWidget(new QLabel { text }, 0, i);
    };

    add_attribute(0, "proficiency", tr("Proficiency"));
    add_attribute(1, "perception", tr("Perception"));
    add_attribute(2, "armorclass", tr("Armor Class"));
    add_attribute(3, "speed", tr("Speed"));
    add_attribute(4, "darkvision", tr("Dark Vision"));

    auto widget = new QWidget {};
    widget->setLayout(layout);
    return widget;
}

QWidget* CharacterTab::create_status_widget() {
    auto layout = new QVBoxLayout {};

    auto hp_layout = new QHBoxLayout {};
    hp_layout->setMargin(0);
    hp_layout->addWidget(new QLabel { tr("<b>Hitpoints</b>:") });

    auto hp_input = new QLineEdit { QString::number(m_character->hitpoints) };
    hp_input->setAlignment(Qt::AlignBaseline | Qt::AlignRight);
    hp_layout->addWidget(hp_input);

    auto hd_layout = new QHBoxLayout {};
    hd_layout->setMargin(0);
    hd_layout->addWidget(new QLabel { tr("<b>Hit Dice</b>:") });

    auto hd_input = new QLineEdit { m_character->hitdice.to_string().c_str() };
    hd_input->setAlignment(Qt::AlignBaseline | Qt::AlignRight);
    hd_layout->addWidget(hd_input);

    auto hp_widget = new QWidget {};
    hp_widget->setLayout(hp_layout);
    layout->addWidget(hp_widget);

    auto hd_widget = new QWidget {};
    hd_widget->setLayout(hd_layout);
    layout->addWidget(hd_widget);

    auto widget = new QWidget {};
    widget->setLayout(layout);
    return widget;
}

QWidget* CharacterTab::create_feature_widget(
        BattleLogWidget* log,
        const dnd5e::Feature& feature) {

    auto layout = new QVBoxLayout {};
    layout->setMargin(0);

    auto head_label = new QLabel { QString { "<b>%1</b>" }.arg(feature.name.c_str()) };
    head_label->setWordWrap(true);
    layout->addWidget(head_label);

    auto description_label = new QLabel { feature.description.c_str() };
    description_label->setWordWrap(true);
    layout->addWidget(description_label);

    if (!feature.roll.empty()) {
        auto box_layout = new QHBoxLayout {};
        box_layout->setAlignment(Qt::AlignLeft | Qt::AlignTop);
        box_layout->setContentsMargins(0, 0, 0, 0);

        auto use_button = new QPushButton { tr("Use") };
        use_button->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        box_layout->addWidget(use_button);

        QObject::connect(use_button, &QPushButton::clicked, [=] {
            log->roll_attack(tr("using"), feature.name.c_str(), feature.roll);
        });

        auto box_widget = new QWidget {};
        box_widget->setLayout(box_layout);
        layout->addWidget(box_widget);
    }

    auto widget = new QWidget {};
    widget->setLayout(layout);
    return widget;
}

QWidget* CharacterTab::create_spell_widget(
        const dnd5e::SpellDescription& spell,
        BattleLogWidget* log) {

    auto layout = new QVBoxLayout {};

    std::map<std::string, std::string> variables {
        { "spell_ability", std::to_string(m_character->magic.attribute_value) },
        { "spell_dc", std::to_string(m_character->magic.difficulty) },
        { "spell_attack", std::to_string(m_character->magic.attack) }
    };

    auto add_button = [=](int level, const std::map<std::string, std::string>& effect) {
        QString tooltip {};
        for (const auto& [key, value] : effect) {
            tooltip += QString { "• <b>%1</b>: %2<br/>" }.arg(key.c_str()).arg(value.c_str());
        }

        if (tooltip.isEmpty()) {
            tooltip = tr("Nothing to roll");
        }

        auto button = new QPushButton { QString::number(level) };
        button->adjustSize();
        button->setFixedWidth(button->height());
        button->setToolTip(tooltip);

        QObject::connect(button, &QPushButton::clicked, [=]() {
            log->roll_attack(
                    tr("casting"),
                    QString { "%1° %2" }.arg(level).arg(spell.name.c_str()),
                    effect,
                    variables);
        });

        return button;
    };

    auto head_label = new QLabel {
        tr("<b>%1</b>: ⛤&nbsp;%2°&nbsp;%3, ⚒︎&nbsp;%4, ↷&nbsp;%5, ⏱&nbsp;%6")
                .arg(spell.name.c_str())
                .arg(spell.level)
                .arg(spell.school.c_str())
                .arg(spell.time.c_str())
                .arg(spell.range.c_str())
                .arg(spell.duration.c_str())
    };
    head_label->setWordWrap(true);
    layout->addWidget(head_label);

    auto components_label = new QLabel {
        tr("Components: %1").arg(spell.components.c_str())
    };
    components_label->setWordWrap(true);
    layout->addWidget(components_label);

    if (!spell.source.empty()) {
        auto source_label = new QLabel { tr("Source: %1").arg(spell.source.c_str()) };
        source_label->setWordWrap(true);
        layout->addWidget(source_label);
    }

    auto description_label = new QLabel { spell.description.c_str() };
    description_label->setWordWrap(true);
    layout->addWidget(description_label);

    auto roll_layout = new QHBoxLayout {};
    roll_layout->setMargin(0);

    roll_layout->addWidget(new QLabel { tr("Cast at level:") });

    for (const auto& [level, effect] : spell.roll) {
        roll_layout->addWidget(add_button(std::stoi(level), effect));
    }

    if (spell.roll.empty()) {
        if (spell.level == 0) {
            roll_layout->addWidget(add_button(spell.level, {}));
        } else {
            for (int level = spell.level; level < 10; ++level) {
                roll_layout->addWidget(add_button(level, {}));
            }
        }
    }

    roll_layout->addStretch(1);

    auto roll_widget = new QWidget {};
    roll_widget->setLayout(roll_layout);
    layout->addWidget(roll_widget);

    auto widget = new QWidget {};
    widget->setLayout(layout);
    return widget;
}

QWidget* CharacterTab::create_features_tab(BattleLogWidget* log) {
    auto content_layout = new QVBoxLayout {};

    auto add_group = [&](const char* when, const QString& title) {
        bool empty = true;
        auto group_layout = new QVBoxLayout {};

        for (const auto& feature : m_character->features) {
            if (feature.when != when) {
                continue;
            }

            group_layout->addWidget(create_feature_widget(log, feature));
            empty = false;
        }

        if (empty) {
            group_layout->addWidget(new QLabel { "None" });
        }

        auto group_widget = new QGroupBox { title };
        group_widget->setLayout(group_layout);

        content_layout->addWidget(group_widget);
    };

    add_group("attack", tr("Attacks"));
    add_group("action", tr("Actions"));
    add_group("bonus", tr("Bonus Actions"));
    add_group("reaction", tr("Reactions"));
    add_group("permanent", tr("Permanent"));
    content_layout->addStretch(1);

    auto content_widget = new QWidget {};
    content_widget->setLayout(content_layout);

    auto widget = new QScrollArea {};
    widget->setWidget(content_widget);
    widget->setWidgetResizable(true);
    return widget;
}

QWidget* CharacterTab::create_equipment_tab() {
    auto layout = new QVBoxLayout {};
    layout->setMargin(0);

    auto content_layout = new QVBoxLayout {};
    content_layout->setMargin(0);

    auto dummy = std::make_unique<QLabel>("1000");
    dummy->adjustSize();

    auto left = QString { "%1" };
    auto right = QString { "<b>%1</b>%2%3%4%5%6%7<i>%8</i>%9%10" };

    for (const auto& equipment : m_character->equipment) {
        auto layout = new QHBoxLayout {};
        layout->setMargin(0);

        auto left_widget = new QLabel { left.arg(equipment.quantity) };
        left_widget->setAlignment(Qt::AlignBaseline | Qt::AlignRight);
        left_widget->setFixedWidth(dummy->width());
        layout->addWidget(left_widget);

        auto right_widget = new QLabel {
            right
                    .arg(equipment.name.c_str())
                    .arg(equipment.worn ? ", worn" : "")
                    .arg(equipment.worth.empty() ? "" : tr(", worth:&nbsp;"))
                    .arg(equipment.worth.c_str())
                    .arg(equipment.weight.empty() ? "" : tr(", weight:&nbsp;"))
                    .arg(equipment.weight.c_str())
                    .arg(equipment.description.empty() ? "" : "<br/>")
                    .arg(equipment.description.c_str())
                    .arg(equipment.comment.empty() ? "" : "<br/>")
                    .arg(equipment.comment.c_str())
        };
        right_widget->setWordWrap(true);
        layout->addWidget(right_widget);

        auto widget = new QWidget {};
        widget->setLayout(layout);
        content_layout->addWidget(widget);
    }

    content_layout->addStretch(1);

    auto content_widget = new QWidget {};
    content_widget->setLayout(content_layout);

    auto scroll_widget = new QScrollArea {};
    scroll_widget->setWidget(content_widget);
    scroll_widget->setWidgetResizable(true);
    layout->addWidget(scroll_widget);

    if (m_character->money != 0) {
        auto money_layout = new QHBoxLayout {};
        money_layout->setMargin(0);

        auto label = new QLabel {
            tr("<b>Money:</b> %1 Gold Pieces, %2 Silver Pieces, %3 Copper Pieces")
                    .arg(m_character->money / 100)
                    .arg((m_character->money / 10) % 10)
                    .arg((m_character->money) % 10)
        };
        label->setWordWrap(true);
        money_layout->addWidget(label);

        auto money_widget = new QWidget {};
        money_widget->setLayout(money_layout);

        layout->addWidget(money_widget);
    }

    auto widget = new QWidget {};
    widget->setLayout(layout);
    return widget;
}

QWidget* CharacterTab::create_magic_tab(BattleLogWidget* log) {
    auto first_layout = new QHBoxLayout {};
    first_layout->setMargin(0);

    first_layout->addWidget(new QLabel {
            tr("Ability: <b>%1</b> (%2)")
                    .arg(m_character->magic.attribute_value)
                    .arg(m_character->magic.attribute_name.c_str()) });
    first_layout->addStretch(1);
    first_layout->addWidget(new QLabel {
            tr("DC: <b>%1</b>").arg(m_character->magic.difficulty) });
    first_layout->addStretch(1);
    first_layout->addWidget(new QLabel {
            tr("Attack: <b>%1</b>").arg(m_character->magic.attack) });

    auto first_widget = new QWidget {};
    first_widget->setLayout(first_layout);

    auto lower_layout = new QVBoxLayout {};
    lower_layout->setMargin(0);

    dnd5e::SpellDescription spell;
    auto find_spell = [&](const std::string& name) {
        for (const auto& s : m_character->magic.spells) {
            if (s.name == name) {
                spell = s;
                return true;
            }
        }

        for (const auto& spellbook : m_character->magic.spellbooks_loaded) {
            for (const auto& s : spellbook) {
                if (s.name == name) {
                    spell = s;
                    return true;
                }
            }
        }

        return false;
    };

    for (const auto& [key, value] : m_character->magic.prepared) {
        if (!value) {
            continue;
        }

        if (find_spell(key)) {
            lower_layout->addWidget(create_spell_widget(spell, log));
            lower_layout->addWidget(new HLineWidget {});
        }
    }

    lower_layout->addStretch(1);

    auto lower_widget = new QWidget {};
    lower_widget->setLayout(lower_layout);

    auto scroll_widget = new QScrollArea {};
    scroll_widget->setWidget(lower_widget);
    scroll_widget->setWidgetResizable(true);

    auto layout = new QVBoxLayout {};
    layout->setMargin(0);
    layout->addWidget(first_widget);
    layout->addWidget(scroll_widget);

    auto widget = new QWidget {};
    widget->setLayout(layout);
    return widget;
}

QWidget* CharacterTab::create_background_tab() {
    auto widget = new QTextEdit {};
    widget->setReadOnly(true);
    widget->setMarkdown(m_character->background.c_str());
    return widget;
}

void MainWindow::removeTab(int index) {
    tabs->removeTab(index);
    saveTabs();
}

void MainWindow::saveTabs() {
    QSettings settings { "dnd5ehelper" };
    settings.remove("tabs");
    settings.beginWriteArray("tabs", tabs->count());
    for (int i = 0; i < tabs->count(); ++i) {
        settings.setArrayIndex(i);
        const auto widget = dynamic_cast<Tab*>(tabs->widget(i));
        if (!widget) {
            continue;
        }
        settings.setValue("type", widget->type);
        settings.setValue("file", widget->filename);
    }
    settings.endArray();
}

void MainWindow::setTheme(QApplication* app, bool dark) {
    QPalette pal;

    if (dark) {
        // clang-format off
        pal.setColor(QPalette::Window,          QColor( 37,  37,  37));
        pal.setColor(QPalette::WindowText,      QColor(212, 212, 212));
        pal.setColor(QPalette::Base,            QColor( 60,  60,  60));
        pal.setColor(QPalette::AlternateBase,   QColor( 45,  45,  45));
        pal.setColor(QPalette::PlaceholderText, QColor(127, 127, 127));
        pal.setColor(QPalette::Text,            QColor(212, 212, 212));
        pal.setColor(QPalette::Button,          QColor( 45,  45,  45));
        pal.setColor(QPalette::ButtonText,      QColor(212, 212, 212));
        pal.setColor(QPalette::BrightText,      QColor(240, 240, 240));
        pal.setColor(QPalette::Highlight,       QColor( 38,  79, 120));
        pal.setColor(QPalette::HighlightedText, QColor(240, 240, 240));
        pal.setColor(QPalette::Light,           QColor( 60,  60,  60));
        pal.setColor(QPalette::Midlight,        QColor( 52,  52,  52));
        pal.setColor(QPalette::Dark,            QColor( 30,  30,  30) );
        pal.setColor(QPalette::Mid,             QColor( 37,  37,  37));
        pal.setColor(QPalette::Shadow,          QColor( 0,    0,   0));
        // clang-format on

        pal.setColor(
                QPalette::Disabled,
                QPalette::Text,
                QColor(127, 127, 127));
    }

    app->setPalette(pal);

    QSettings settings { "dnd5ehelper" };
    settings.setValue("darkmode", dark);
}

MainWindow::MainWindow(QApplication* app) :
    QMainWindow {} {
    QSettings settings { "dnd5ehelper" };

    setTheme(app, settings.value("darkmode").toBool());

    tabs = new QTabWidget {};
    tabs->setMovable(true);
    tabs->setTabsClosable(true);

    QObject::connect(
            tabs->tabBar(),
            &QTabBar::tabCloseRequested,
            this,
            &MainWindow::removeTab);

    QObject::connect(
            tabs->tabBar(),
            &QTabBar::tabMoved,
            this,
            &MainWindow::saveTabs);

    const auto count = settings.beginReadArray("tabs");
    for (int i = 0; i < count; ++i) {
        settings.setArrayIndex(i);
        openTab(
                settings.value("type").toInt(),
                settings.value("file").toString(),
                false);
    }
    settings.endArray();

    auto* pgup = new QShortcut { QKeySequence { "Ctrl+PgUp" }, tabs };
    pgup->connect(pgup, &QShortcut::activated, [=] {
        tabs->setCurrentIndex(
                (tabs->currentIndex() + tabs->count() - 1) % tabs->count());
    });

    auto* pgdown = new QShortcut { QKeySequence { "Ctrl+PgDown" }, tabs };
    pgdown->connect(pgdown, &QShortcut::activated, [=] {
        tabs->setCurrentIndex((tabs->currentIndex() + 1) % tabs->count());
    });

    setCentralWidget(tabs);

    auto menubar = new QMenuBar {};
    setMenuBar(menubar);

    auto filemenu = menubar->addMenu("&File");

    auto fileopenmenu = filemenu->addMenu("&Open");

    QObject::connect(
            fileopenmenu->addAction("&Character"),
            &QAction::triggered,
            [&]() {
                auto filename = QFileDialog::getOpenFileName(
                        nullptr,
                        "Open Character",
                        "",
                        "Character Files (*.json *.yaml)");
                openTab('c', filename);
            });

    QObject::connect(
            fileopenmenu->addAction("Cheat&sheet"),
            &QAction::triggered,
            [&]() {
                auto filename = QFileDialog::getOpenFileName(
                        nullptr,
                        "Open Cheatsheet",
                        "",
                        "Cheatsheet Files (*.md *.html *.txt)");
                openTab('s', filename);
            });

    QObject::connect(
            fileopenmenu->addAction("&Notes"),
            &QAction::triggered,
            [&]() {
                auto filename = QFileDialog::getOpenFileName(
                        nullptr,
                        "Open Notes",
                        "",
                        "Notes Files (*.txt)");
                openTab('n', filename);
            });

    QObject::connect(
            fileopenmenu->addAction("&Monsterbook"),
            &QAction::triggered,
            [&]() {
                auto filename = QFileDialog::getOpenFileName(
                        nullptr,
                        "Open Monsterbook",
                        "",
                        "Monsterbook Files (*.json *.yaml)");
                openTab('m', filename);
            });

    QObject::connect(
            fileopenmenu->addAction("Spell&book"),
            &QAction::triggered,
            [&]() {
                auto filename = QFileDialog::getOpenFileName(
                        nullptr,
                        "Open Spellbook",
                        "",
                        "Spellbook Files (*.json *.yaml)");
                openTab('b', filename);
            });

    filemenu->addSeparator();

    QObject::connect(
            filemenu->addAction("&Quit"),
            &QAction::triggered,
            app,
            &QApplication::quit);

    auto optionsmenu = menubar->addMenu("&Options");

    auto darkmode = optionsmenu->addAction("Dark Mode");
    darkmode->setCheckable(true);
    darkmode->setChecked(settings.value("darkmode").toBool());
    QObject::connect(
            darkmode,
            &QAction::triggered,
            [=]() { setTheme(app, darkmode->isChecked()); });
}

void MainWindow::openTab(char type, const QString& path, bool update) {
    if (path.isEmpty()) {
        return;
    }

    const auto filename = QFileInfo { path }.fileName();

    switch (type) {
    case 'b':
        tabs->addTab(new SpellBookTab { path }, filename);
        break;
    case 's':
        tabs->addTab(new CheatSheetTab { path }, filename);
        break;
    case 'n':
        tabs->addTab(new NotesTab { path }, filename);
        break;
    case 'm':
        tabs->addTab(new MonsterBookTab { path }, filename);
        break;
    case 'c':
        tabs->addTab(new CharacterTab { path }, filename);
        break;
    default:
        return;
    }

    if (update) {
        saveTabs();
    }
}

int gui(int argc, char* argv[]) {
    QApplication app { argc, argv };

    MainWindow window { &app };
    window.show();

    return app.exec();
}
