// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake
#include "dnd5ehelper.h"

#include "database.h"
#include "dice.h"

#if ENABLE_TESTS
#include <gtest/gtest.h>
#endif /* ENABLE_TESTS */

#include <fstream>
#include <iostream>

static void usage(std::ostream& stream, const char* name) {
    stream
            << "Usage: " << name << " ACTION\n"
            << "Actions:\n"
            << "  help          Display this help\n"
            << "  roll DICE     Roll dices such as \"2d6+1\"\n"
            << "  atk BONUS DICE\n"
            << "                Roll a D&D5e attack roll\n"
            << "  collate FILE...\n"
            << "                Collate monster files into single database\n"
            << "  gui           Display gui. This is the default\n"
#if ENABLE_TESTS
            << "  selftest      Run self tests\n"
#endif
            << "\nD&D5e Helper (c) 2021 Tim Wiederhake\n"
            << "Report bugs to "
            << "https://gitlab.com/twied/dnd5ehelper/issues.\n";
}

int roll(int argc, char* argv[]) {
    std::string string {};
    for (int i = 2; i < argc; ++i) {
        string += argv[i];
    }

    auto dices = dnd5e::DiceSet::parse(string);
    auto roll = dices.roll(dnd5e::Random::instance());

    std::cout
            << "Rolling \""
            << dices.to_string()
            << "\":\n"
            << roll.to_string()
            << ".\n";

    return 0;
}

int atk(int argc, char* argv[]) {
    if (argc < 3) {
        usage(std::cerr, argv[0]);
        return 1;
    }

    std::string string {};
    for (int i = 3; i < argc; ++i) {
        string += argv[i];
    }

    auto bonus = std::atoi(argv[2]);
    auto damage = dnd5e::DiceSet::parse(string);
    auto attack = dnd5e::Random::instance()(1, 20);

    std::cout
            << "Attacking "
            << (bonus < 0 ? "" : "+")
            << bonus
            << " \""
            << damage.to_string()
            << "\":\n";

    std::cout
            << attack
            << "/20"
            << (bonus < 0 ? " - " : " + ")
            << std::abs(bonus)
            << " = ";

    if (attack == 1) {
        std::cout << "FUMBLE\n";
        dnd5e::DiceSet fumble {};
        for (const auto& [sides, amount] : damage) {
            fumble.add_dice(sides, -amount);
        }
        std::swap(damage, fumble);
    } else if (attack == 20) {
        std::cout << "CRITICAL\n";
        dnd5e::DiceSet critical {};
        for (const auto& [sides, amount] : damage) {
            critical.add_dice(sides, amount * (sides == 1 ? 1 : 2));
        }
        std::swap(damage, critical);
    } else {
        std::cout << (attack + bonus) << " to hit\n";
    }

    auto roll = damage.roll(dnd5e::Random::instance());
    std::cout << roll.to_string() << ".\n";
    return 0;
}

int collate(int argc, char* argv[]) {
    std::vector<dnd5e::Monster> monsters {};
    for (int i = 2; i < argc; ++i) {
        std::ifstream stream { argv[i] };
        stream >> monsters;
    }

    std::cout << monsters << '\n';
    return 0;
}

int main(int argc, char* argv[]) {
    std::string action { argc < 2 ? "gui" : argv[1] };

    if (action == "help" || action == "--help" || action == "-h") {
        usage(std::cout, argv[0]);
        return 0;
    }

#if ENABLE_TESTS
    if (action == "selftest") {
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    }
#endif /* ENABLE_TESTS */

    if (action == "roll") {
        return roll(argc, argv);
    }

    if (action == "atk") {
        return atk(argc, argv);
    }

    if (action == "collate") {
        return collate(argc, argv);
    }

    if (action == "gui") {
        return gui(argc, argv);
    }

    usage(std::cerr, argv[0]);
    return 1;
}
