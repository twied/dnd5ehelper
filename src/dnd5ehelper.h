// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#ifndef DND5EHELPER_H_
#define DND5EHELPER_H_

int roll(int argc, char* argv[]);
int atk(int argc, char* argv[]);
int collate(int argc, char* argv[]);
int gui(int argc, char* argv[]);

#endif /* DND5EHELPER_H_ */
