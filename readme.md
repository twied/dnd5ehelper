This is a small D&D DM helper that I wrote during the 2020 / 2021 pandemic,
to help me in my virtual campaings. While players only have to keep one class
and one set of attacks, spells, etc. in mind, the DM has to know the stats of
a lot of monsters.

The main goal of this tool is to speed up combat, to get the players more
spotlight and less waiting for the DM to look up what excatly they have to roll
for a particular action.

![screenshot](examples/screenshot.png)

Compilation
===========

This is a meson based project. To build, run:
```
$ meson build
$ ninja -C build
```

To run the test suite, append `-DENABLE_TESTS=true` to the `meson build` call,
and execute `dndhelper selftest` or `ninja -C build test`.

License
=======

This project is licensed under the terms of the GNU General Public License
(GNU GPL) version 2 or later.
